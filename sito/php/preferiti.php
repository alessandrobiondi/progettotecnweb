<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "Scalper-Preferiti";
$templateParams["nome"] = "Eventi/preferiti.php";
if (isUserLoggedIn()) {
    if (isset($_SESSION["IdUtente"])) {
        $id = $_SESSION["IdUtente"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdUtente($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
        $templateParams["carrello"] = $dbh->getCarrelloByUtente($id);
        $templateParams["preferiti"] = $dbh->getPreferitiByIdUtente($id);
        $i = 0;
        foreach ($templateParams["preferiti"] as $preferito) {
            $templateParams["eventi"][$i] = $dbh->getEventobyID($preferito["IdEvento"]);
            $i++;
        }
    }
}
require("template/base.php");
