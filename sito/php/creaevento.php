<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "Scalper-Crea-Evento";
$templateParams["nome"] = "Eventi/creaevento.php";
$templateParams["elencocategorie"] = $dbh->getCategorie();
if (isOrganizerUserLoggedIn()) {
    if (isset($_SESSION["IdOrganizzatore"])) {
        $id = $_SESSION["IdOrganizzatore"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdOrganizzatore($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }
}
require("template/base.php");
