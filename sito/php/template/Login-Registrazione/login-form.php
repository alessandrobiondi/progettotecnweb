        <form action="#" method="POST">
            <h2>Login</h2>
            <div class="form-group">
                <label for="mail">Email address</label>
                <input type="email" class="form-control" id="mail" name="mail" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name = "password" placeholder="Password">
            </div>
            <div>
            <button type="submit" name="submit" class="btn btn-primary">Login</button>
            </div>  
            <label></label>
        </form>
        <div>
        <label>Non sei registrato?</label><br>
            </div>
        <button name="registrazione" class="btn btn-primary" onclick=newUser()>Registrati</button><br><br>
        <button name="registrazioneOrg" class="btn btn-primary" onclick=newOrgUser()>Registrati come organizzatore</button><br>