<?php if (isset($templateParams["titolo_pagina"])) : ?>
    <h2><?php echo $templateParams["titolo_pagina"]; ?></h2>
<?php endif; ?>
<?php if (isset($templateParams["eventi"])) : ?>
    <ul id="eventList">
        <?php foreach ($templateParams["eventi"] as $evento) : ?>
            <li>
                <a href="dettaglioevento.php?id=<?php echo $evento[0]["IdEvento"]; ?>">
                    <article class="evento">
                        <header>
                            <h2><?php echo $evento[0]["Titolo"]; ?></h2>
                        </header>

                        <div class="imgEvento">
                            <img src="<?php echo UPLOAD_DIR . $evento[0]["ImgEvento"]; ?>" alt="" />
                        </div>

                        <div class="descrizioneEvento">
                            <section>
                                <h3>
                                    <?php $newDate = date("d-m-Y", strtotime($evento[0]["Data"]));
                                    echo $newDate ?>
                                </h3>

                                <p><?php echo $evento[0]["Anteprima"]; ?></p>
                            </section>
                        </div>
                    </article>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
<?php else : ?>
    <h2> Non ci sono preferiti </h2>
<?php endif; ?>