<?php if (count($templateParams["evento"]) == 0) : ?>
    <article>
        <p>Evento non presente</p>
    </article>
<?php
else :
    $evento = $templateParams["evento"][0];
    $nomesocieta = $templateParams["nomesocieta"][0];
    $categoria = $templateParams["categoria"][0];
?>
    <article class="dettaglioevento">
        <div id="container">
            <img src="<?php echo UPLOAD_DIR . $evento["ImgEvento"]; ?>" alt="" />
        </div>
        <header>
            <h1><?php echo $evento["Titolo"]; ?></h1>
        </header>
        <div id="informazioniEvento">
            <p><strong>Luogo:</strong> <?php echo $evento["Luogo"]; ?></p>
            <p><strong>Data:</strong> <?php $newDate = date("d-m-Y", strtotime($evento["Data"]));
                                        echo $newDate ?></p>
            <p><strong>Categoria:</strong> <?php echo $categoria["Descrizione"]; ?></p>
            <p><strong>Organizzatore:</strong> <?php echo $nomesocieta["NomeSocieta"]; ?></p>
            <p><strong>Posti disponibili:</strong> <?php echo $evento["BigliettiDisponibili"]; ?></p>
            <p><strong>Posti prenotati:</strong> <?php echo $templateParams["postiPrenotati"]; ?></p>
        </div>
        <section>
            <div>
                <h2>Descrizione</h2>
                <p><?php echo $evento["Descrizione"]; ?></p>
            </div>
        </section>
        <div id="numerobiglietti">
            <p><strong>Prezzo: </strong><?php echo $evento["Prezzo"]; ?> </p>
        </div>
        <form action="modificaevento.php" method="post">
            <input type="hidden" name="id" value="<?php echo $evento['IdEvento']; ?>" />
            <button class="btn btn-primary">Modifica evento</button>
        </form>
    </article>
<?php endif; ?>