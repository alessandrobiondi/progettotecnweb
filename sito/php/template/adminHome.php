<label><strong>AMMINISTRATORE, </strong><?php echo $_SESSION["Nome"] ?></label>
<br>
<div class="container">
  <button class="btn btn-primary" id="btnLogout" onclick=logout()>Logout</button>
</div>
<br>
<?php
$organizers = $dbh->getNotAcceptedOrg();
if (count($organizers) != 0) :
?>
  </div>
  <div>
    <br><label>Accetta nuovi organizzatori</label>
  </div>
  <div>
  <?php
  $i = 0;
  $organizers = $dbh->getNotAcceptedOrg();

  while ($i < count($organizers)) {
    echo "<div id=" . $i . ' ' . "class=" . '"' . "alert" . '"' . ">";

    $j = $i + 1;
    echo "<span id='y" . $i . "'class=" . ' "closebtn" ' . " onclick=" . "accettaOrg(" . $organizers[$i]["IdOrganizzatore"] . "," . $i . ")" . ">&check;</span>";
    echo "<span id='x" . $i . "'class=" . ' "closebtn" ' . " onclick=" . "rifiutaOrg(" . $organizers[$i]["IdOrganizzatore"] . "," . $i . ")" . ">&times;</span>";
    echo "ID: " . $organizers[$i]["IdOrganizzatore"] . "<br>";
    echo "Nome: " . $organizers[$i]["Nome"] . " " . $organizers[$i]["Cognome"] . "<br>";
    echo "Società: " . $organizers[$i]["NomeSocieta"] . "<br>";
    echo "Mail: " . $organizers[$i]["Mail"] . "</div>";
    $i++;
  } else :
  echo "<label>Non ci sono nuove richieste</label>";
endif;
  ?>
  <div class="form-row">
    <div class="form-group col-md-12" id="aggiungiCat">
      <label for="categoria">Nuova categoria</label>
      <input type="text" class="form-control" id="categoria" placeholder="Categoria"><br>
      <button class="btn btn-primary" id="btnAddCat" onclick=aggiungiCategoria()>Aggiungi categoria</button>
    </div>
    <div class="container">
      <button type="button" id="btnModificaPsw" onclick="vaiModificaPassword()" class="btn btn-primary">Modifica Password</button>
    </div>
  </div>