<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "Scalper-Dettaglio-Evento";

$idevento = -1;
if(isset($_GET["id"])){
    $idevento = $_GET["id"];
}

if(isOrganizerUserLoggedIn() || isAdminUserLoggedIn()){
    $templateParams["nome"] = "Eventi/dettaglioeventoOrganizzatore.php";
    $templateParams["evento"] = $dbh->getEventobyID($idevento);
    $evento = $templateParams["evento"][0];
    $templateParams["nomesocieta"] = $dbh->getNomeSocietabyID($evento["IdOrganizzatore"]);
    $templateParams["categoria"] = $dbh->getDescrizioneCategoriabyID($evento["IdCategoria"]);
    $templateParams["postiPrenotati"]=$dbh->getPostiPrenotatibyIDEvento($idevento);
    if (isset($_SESSION["IdOrganizzatore"])) {
        $id = $_SESSION["IdOrganizzatore"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdOrganizzatore($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }elseif (isset($_SESSION["IdAmministratore"])){
        $id = $_SESSION["IdAmministratore"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdAmministratore($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }
}
else {
    $templateParams["nome"] = "Eventi/dettaglioevento.php";
    $templateParams["evento"] = $dbh->getEventobyID($idevento);
    $evento = $templateParams["evento"][0];
    $templateParams["nomesocieta"] = $dbh->getNomeSocietabyID($evento["IdOrganizzatore"]);
    $templateParams["categoria"] = $dbh->getDescrizioneCategoriabyID($evento["IdCategoria"]);
    $templateParams["postiPrenotati"]=$dbh->getPostiPrenotatibyIDEvento($idevento);
    if (isset($_SESSION["IdUtente"])) {
        $id = $_SESSION["IdUtente"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdUtente($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
        $templateParams["carrello"] = $dbh->getCarrelloByUtente($id);
        $templateParams["preferiti"] = $dbh->getPreferitiByIdUtente($id);
        $bool = false;
        foreach ($templateParams["preferiti"] as $preferito) {
            $templateParams["eventotemp"] = $dbh->getEventobyID($preferito["IdEvento"]);
            $eventotemp = $templateParams["eventotemp"][0];
            if($evento["IdEvento"] == $eventotemp["IdEvento"]){
                $bool = true;
            }
        }
        $templateParams["bool"] = $bool;

        $bool2 = false;
        foreach ($templateParams["carrello"] as $elemento) {
            $templateParams["eventotemp"] = $dbh->getEventobyID($elemento["IdEvento"]);
            $eventotemp = $templateParams["eventotemp"][0];
            if($evento["IdEvento"] == $eventotemp["IdEvento"]){
                $bool2 = true;
            }
        }
        $templateParams["bool2"] = $bool2;
    }
}

require("template/base.php");
