function checkcreate(ev) {
    let nome = document.getElementById("inputNomeEvento").value;
    let luogo = document.getElementById("inputLuogo").value;
    let data = document.getElementById("inputData").value;
    let e = document.getElementById("inputCategoria");
    let categoria = e.options[e.selectedIndex].value;
    let categoriaid = parseInt(categoria);
    let descrizione = document.getElementById("inputDescrizione").value;
    let posti = document.getElementById("inputPosti").value;
    let prezzo = document.getElementById("inputPrezzo").value;
    let img = document.querySelector('input[type=file]').files[0];
    let anteprima = document.getElementById("inputAnteprima").value;

    if (nome != "" && luogo != "" && data != "" && !isNaN(categoriaid) && descrizione != "" && posti != "" && prezzo != "" && anteprima != "" && img != "undefined") {
        return true;
    }
    else {
        if (nome == "") {
            document.getElementById("inputNomeEvento").style.border = "1px solid red";
        }
        if (luogo == "") {
            document.getElementById("inputLuogo").style.border = "1px solid red";
        }
        if (data == "") {
            document.getElementById("inputData").style.border = "1px solid red";
        }
        if (isNaN(categoriaid)) {
            document.getElementById("inputCategoria").style.border = "1px solid red";
        }
        if (descrizione == "") {
            document.getElementById("inputDescrizione").style.border = "1px solid red";
        }
        if (posti == "") {
            document.getElementById("inputPosti").style.border = "1px solid red";
        }
        if (prezzo == "") {
            document.getElementById("inputPrezzo").style.border = "1px solid red";
        }
        if (anteprima == "") {
            document.getElementById("inputAnteprima").style.border = "1px solid red";
        }
        if (typeof img === 'undefined') {
            document.getElementById("alertImg").style.display = "block";
        }
        else {
            document.getElementById("alertImg").style.display = "none";
        }

        return false;
    }
}

function checkupdate() {
    let nome = document.getElementById("inputNomeEvento").value;
    let luogo = document.getElementById("inputLuogo").value;
    let data = document.getElementById("inputData").value;
    let e = document.getElementById("inputCategoria");
    let categoria = e.options[e.selectedIndex].value;
    let categoriaid = parseInt(categoria);
    let descrizione = document.getElementById("inputDescrizione").value;
    let anteprima = document.getElementById("inputAnteprima").value;

    if (nome != "" && luogo != "" && data != "" && !isNaN(categoriaid) && descrizione != "" && anteprima != "") {
        return true;
    }
    else {
        if (nome == "") {
            document.getElementById("inputNomeEvento").style.border = "1px solid red";
        }
        if (luogo == "") {
            document.getElementById("inputLuogo").style.border = "1px solid red";
        }
        if (data == "") {
            document.getElementById("inputData").style.border = "1px solid red";
        }
        if (isNaN(categoriaid)) {
            document.getElementById("inputCategoria").style.border = "1px solid red";
        }
        if (descrizione == "") {
            document.getElementById("inputDescrizione").style.border = "1px solid red";
        }
        if (anteprima == "") {
            document.getElementById("inputAnteprima").style.border = "1px solid red";
        }

        return false;
    }
}