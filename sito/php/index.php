<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "Scalper-Home";
$templateParams["nome"] = "home.php";
if(isset($_GET["IdCategoria"])){
    $templateParams["eventi"]=$dbh->getEventsByCategory($_GET["IdCategoria"]);
}elseif(isset($_GET["Titolo"])){
    $templateParams["eventi"]=$dbh->getEventsByFilter($_GET["Titolo"]);
}else{
    if (isOrganizerUserLoggedIn()) {
        if (isset($_SESSION["IdOrganizzatore"])) {
            $id = $_SESSION["IdOrganizzatore"];
            $templateParams["eventi"] = $dbh->getEventsByIdOrganizer($id);
        }
    }else {
        $templateParams["eventi"] = $dbh->getAllEvents();
    }
}
if (isOrganizerUserLoggedIn()) {
    if (isset($_SESSION["IdOrganizzatore"])) {
        $id = $_SESSION["IdOrganizzatore"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdOrganizzatore($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }
}elseif (isUserLoggedIn()) {
    if (isset($_SESSION["IdUtente"])) {
        $id = $_SESSION["IdUtente"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdUtente($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
        $templateParams["carrello"] = $dbh->getCarrelloByUtente($id);
    }
}elseif (isAdminUserLoggedIn()) {
    if (isset($_SESSION["IdAmministratore"])) {
        $id = $_SESSION["IdAmministratore"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdAmministratore($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }
}
require("template/base.php");
?>