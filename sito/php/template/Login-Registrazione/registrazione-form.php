    <form action="registrazioneStandard.php" method="post" enctype="multipart/form-data" onsubmit="return checkNewUser()">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputNome">Nome</label>
                <input type="text" class="form-control" name="inputNome" id="inputNome" placeholder="Nome">
            </div>
            <div class="form-group col-md-6">
                <label for="inputCognome">Cognome</label>
                <input type="text" class="form-control" name="inputCognome" id="inputCognome" placeholder="Cognome">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputMail">Mail</label>
                <input type="text" class="form-control" name="inputMail" id="inputMail" placeholder="Mail">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword">Password</label>
                <input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Password">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputIndirizzo">Indirizzo</label>
                <input type="text" class="form-control" name="inputIndirizzo" id="inputIndirizzo" placeholder="Indirizzo">
            </div>
            <div class="form-group col-md-3">
                <label for="inputCittà">Citta'</label>
                <input type="text" class="form-control" name="inputCittà" id="inputCittà" placeholder="Città">
            </div>
            <div class="form-group col-md-3">
                <label for="inputCAP">CAP</label>
                <input type="text" class="form-control" name="inputCAP" id="inputCAP" placeholder="CAP">
            </div>
        </div>
        <div>
            <input type="submit" class="btn btn-primary" value="Registrati">
        </div>
    </form>