<?php if (count($templateParams["elencocategorie"]) == 0) : ?>
    <article>
        <p>Errore nel caricamento delle categorie</p>
    </article>
<?php
else :
    $evento = $templateParams["evento"][0];
?>
    <form action="updateEvento.php" method="post" enctype="multipart/form-data" onsubmit="return checkupdate()">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputNomeEvento">Nome evento</label>
                <?php echo '<input type="text" class="form-control" name="inputNomeEvento" id="inputNomeEvento" value="' . $evento["Titolo"] . '">'; ?>
            </div>
            <div class="form-group col-md-6">
                <label for="inputLuogo">Luogo</label>
                <?php echo '<input type="text" class="form-control" name="inputLuogo" id="inputLuogo" value="' . $evento["Luogo"] . '">'; ?>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputData">Data</label>
                <?php echo '<input type="date" class="form-control" name="inputData" id="inputData" value="' . $evento["Data"] . '">'; ?>
            </div>
            <div class="form-group col-md-6">
                <label for="inputCategoria">Categoria</label>
                <select name="inputCategoria" id="inputCategoria" style="width: 100%; height: calc(1.5em + .75rem + 2px);">
                    <option value="none" selected disabled hidden>
                        Selezionare una categoria
                    </option>
                    <?php for ($i = 0; $i < count($templateParams["elencocategorie"]); ++$i) {
                        $categoria = $templateParams["elencocategorie"][$i];
                        if ($evento["IdCategoria"] == $i + 1) {
                            echo '<option value="' . $i . '" selected="selected">' . $categoria['Descrizione'] . '</option>';
                        } else {
                            echo '<option value="' . $i . '">' . $categoria['Descrizione'] . '</option>';
                        }
                    } ?>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputAnteprima">Anteprima</label>
                <?php echo '<input type="text" class="form-control" name="inputAnteprima" id="inputAnteprima" value="' . $evento["Anteprima"] . '">'; ?>
            </div>
            <div class="form-group col-md-6">
                <label for="inputDescrizione">Descrizione</label>
                <?php echo '<input type="text" class="form-control" name="inputDescrizione" id="inputDescrizione" value="' . $evento["Descrizione"] . '">'; ?>
            </div>
        </div>
        <div class="upload-btn-div">
            <input type="file" name="ImgEvento" id="ImgEvento" title="imgEvento" accept="image/*" />
            <button id="buttonImg" class="btn btn-primary">Modifica immagine</button>
        </div>
        <div>
            <input type="submit" class="btn btn-primary" value="Salva modifiche">
        </div>
        <input type="hidden" name="id" value="<?php echo $evento['IdEvento']; ?>" />
    </form>
<?php endif; ?>