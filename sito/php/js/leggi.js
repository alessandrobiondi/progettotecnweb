function leggiNotificaOrganizzatore(id, i) {
    $.post("leggiOrganizzatore.php", { idNotifica: id });
    document.getElementById(i).className = "alert-letto";
    document.getElementById("x" + i).style.display = "none";
    modificaIcona();
}

function leggiNotificaUtente(id, i) {
    $.post("leggiUtente.php", { idNotifica: id });
    document.getElementById(i).className = "alert-letto";
    document.getElementById("x" + i).style.display = "none";
    modificaIcona();
}

function leggiNotificaAmministratore(id, i) {
    $.post("leggiAmministratore.php", { idNotifica: id });
    document.getElementById(i).className = "alert-letto";
    document.getElementById("x" + i).style.display = "none";
    modificaIcona();
}

function modificaIcona() {
    let badge = document.getElementById("badgeNotifica");
    let n = badge.textContent;
    n--;
    if (n != 0) {
        badge.textContent = n;
    } else {
        document.getElementById("notificaIcon").className = "far fa-bell";
        badge.style.display = "none";
    }
}