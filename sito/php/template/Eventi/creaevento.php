<?php if (count($templateParams["elencocategorie"]) == 0) : ?>
    <article>
        <p>Errore nel caricamento delle categorie</p>
    </article>
<?php
else :
?>
    <form action="insertEvento.php" method="post" enctype="multipart/form-data" onsubmit="return checkcreate()">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputNomeEvento">Nome evento</label>
                <input type="text" class="form-control" name="inputNomeEvento" id="inputNomeEvento" placeholder="Nome evento">
            </div>
            <div class="form-group col-md-6">
                <label for="inputLuogo">Luogo</label>
                <input type="text" class="form-control" name="inputLuogo" id="inputLuogo" placeholder="Luogo">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputData">Data</label>
                <input type="date" class="form-control" name="inputData" id="inputData">
            </div>
            <div class="form-group col-md-6">
                <label for="inputCategoria">Categoria</label>
                <select name="inputCategoria" id="inputCategoria" style="width: 100%; height: calc(1.5em + .75rem + 2px);">
                    <option value="none" selected disabled hidden>
                        Selezionare una categoria
                    </option>
                    <?php for ($i = 0; $i < count($templateParams["elencocategorie"]); ++$i) {
                        $categoria = $templateParams["elencocategorie"][$i];
                        echo '<option value="' . $i . '">' . $categoria['Descrizione'] . '</option>';
                    } ?>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="inputAnteprima">Anteprima</label>
                <input type="text" class="form-control" name="inputAnteprima" id="inputAnteprima" placeholder="Anteprima (MAX 200 caratteri)">
            </div>
            <div class="form-group col-md-6">
                <label for="inputDescrizione">Descrizione</label>
                <input type="text" class="form-control" name="inputDescrizione" id="inputDescrizione" placeholder="Descrizione">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputPosti">Posti disponibili</label>
                <input type="number" class="form-control" name="inputPosti" id="inputPosti" placeholder="Posti">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPrezzo">Prezzo biglietto</label>
                <input type="number" placeholder="0" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="inputPrezzo" name="inputPrezzo" />
            </div>
        </div>
        <p id="alertImg" style="color:red; display:none">È necesario caricare un'immagine</p>
        <div class="upload-btn-div">
            <button id="buttonImg" class="btn btn-primary">Carica immagine</button>
            <input type="file" name="ImgEvento" id="ImgEvento" title="imgEvento" accept="image/*" />
        </div>
        <div>
            <input type="submit" class="btn btn-primary" value="Crea Evento">
        </div>
    </form>
<?php endif; ?>