<?php if (count($templateParams["notifiche"]) == 0) : ?>
  <article>
    <h2>Nessuna notifica</h2>
  </article>
<?php
else :
  $notifiche = $templateParams["notifiche"];
?>
  <label>Notifiche di <?php echo $_SESSION["Nome"] ?></label>
  <?php
  $i = 0;
  while ($i < count($notifiche)) {
    if ($notifiche[$i]["Letto"] == 1) {
      echo "<div id=" . $i . ' ' . "class=" . '"' . "alert-letto" . '"' . ">";
      echo "" . $notifiche[$i]["Messaggio"] . "</div>";
    } else {
      echo "<div id=" . $i . ' ' . "class=" . '"' . "alert" . '"' . ">";
      echo "<span id='x" . $i . "'class=" . ' "closebtn" ' . " onclick=" . "leggiNotificaAmministratore(" . $notifiche[$i]["IdNotifica"] . "," . $i . ")" . ">&times;</span>";
      echo "" . $notifiche[$i]["Messaggio"] . "</div>";
    }
    $i++;
  }
  ?>
<?php endif; ?>