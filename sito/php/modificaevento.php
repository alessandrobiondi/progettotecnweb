<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "Scalper-Modifica Evento";
$templateParams["nome"] = "Eventi/modificaevento.php";
$templateParams["elencocategorie"] = $dbh->getCategorie();

$idevento = -1;
if(isset($_POST["id"])){
    $idevento = $_POST["id"];;
}
if (isOrganizerUserLoggedIn()) {
    if (isset($_SESSION["IdOrganizzatore"])) {
        $id = $_SESSION["IdOrganizzatore"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdOrganizzatore($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }
}

$templateParams["evento"] = $dbh->getEventobyID($idevento);

require("template/base.php");
?>