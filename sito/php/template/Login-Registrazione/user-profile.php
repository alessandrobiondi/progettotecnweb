<div>
  <label><strong>Benvenuto</strong>, <?php echo $_SESSION["Nome"] ?></label>
</div>

<div class="container">
  <button type="button" id="btnLogout" onclick="logout()" class="btn btn-primary">Logout</button>
</div>
<br>
<div class="container">
  <button type="button" id="btnPreferiti" onclick="vaiPreferiti()" class="btn btn-primary">Preferiti</button>
</div>
<?php
    $ordini= $dbh->getOrdiniByIdUtente($_SESSION["IdUtente"]);
    if(count($ordini)!=0):
?>
<div>
  <br><label>I tuoi ordini</label>
</div>
  <?php
    $i=0; 
    $ordini= $dbh->getOrdiniByIdUtente($_SESSION["IdUtente"]);
    while ($i < count($ordini)) {
      echo "<div id=" . $i . ' '."class=".'"' . "alert".'"'.">";
      $j=$i+1;
      echo "Data acquisto: " . $ordini[$i]["DataAcquisto"]."<br>";
      echo "<strong>"."Data evento: " .$ordini[$i]["Data"]."</strong><br>";
      echo "<strong>".$ordini[$i]["Titolo"]."</strong><br>";
      echo "Biglietti acquistati: ".$ordini[$i]["NumeroBiglietti"]."<br>";
      echo "Totale: ".$ordini[$i]["Totale"]." €"."</div>";
      $i++;
    }
    else:
    echo "<label>Non hai effettuato ordini</label><br>";
    endif;
  ?>
<label></label>

<br>
<div class="container">
  <button type="button" id="btnModificaPsw" onclick="vaiModificaPassword()" class="btn btn-primary">Modifica Password</button>
</div>