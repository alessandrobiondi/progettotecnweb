<?php
require_once("bootstrap.php");

$templateParams["elencocategorie"] = $dbh->getCategorie();
if (isUserLoggedIn()) {
    $templateParams["titolo"] = "Scalper-Pagamento";
    $templateParams["nome"] = "Carrello/pagamento.php";
    if (isset($_SESSION["IdUtente"])) {
        $id = $_SESSION["IdUtente"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdUtente($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
        $templateParams["carrello"] = $dbh->getCarrelloByUtente($id);
        if(count($templateParams["carrello"])==0){
        
            $templateParams["nome"] = "Carrello/acquistoAvvenuto.php";
            $templateParams["titolo"] = "Scalper-Carrello";
        }
        else{
            $templateParams["Totale"] = $dbh->getTotal($_SESSION["IdUtente"]);
        }
        
    }
}
require("template/base.php");
?>
