<div id="compra">
    <h3>Totale provvisorio: <?php echo $templateParams["Totale"][0]["SUM(Totale)"]; ?>€</h3>
    <button class="btn btn-primary" onclick=procediConLOrdine()>Procedi con l'ordine</button>
</div>
<div>
    <?php if (isset($templateParams["titolo_pagina"])) : ?>
        <h2><?php echo $templateParams["titolo_pagina"]; ?></h2>
    <?php endif; ?>
    <?php $j = 0;
    foreach ($templateParams["carrello"] as $carrello) : ?>
        <?php
        require_once("bootstrap.php");
        $templateParams["evento"] = $dbh->getEventobyID($carrello["IdEvento"]);
        $evento = $templateParams["evento"][0];
        $templateParams["organizzatore"] = $dbh->getOrganizzatoreByID($evento["IdOrganizzatore"]);
        $organizzatore = $templateParams["organizzatore"][0];
        ?>
        <article id="eventoCar">
            <header>
                <h2><?php echo $evento["Titolo"]; ?></h2>
            </header>
                    <img class="imgCar" alt="" src="<?php echo UPLOAD_DIR . $evento["ImgEvento"]; ?>"  />
                
                <section>
                    <h3>Dettagli</h3>
                    <p>Luogo: <?php echo $evento["Luogo"] ?></p>
                    <p>Data: <?php echo $evento["Data"] ?></p>
                    <p>Organizzato da: <?php echo $organizzatore["NomeSocieta"] ?></p>
                </section>
                <section>
                    <h4>Spesa</h4>
                    <div id="prezz">
                        <p>Prezzo per biglietto: </p>
                        <p id="prezzoBiglietto<?php echo $j ?>"><?php echo $evento["Prezzo"] ?></p>
                        <p>€</p>
                    </div>
                    <p>
                    <label for="quantità<?php echo $j ?>">Q.tà:</label>
                    <select name="selectquantità" id="quantità<?php echo $j ?>" onchange="modificaQuantità(<?php echo $j ?>);">

                        <?php require_once("bootstrap.php");
                        $templateParams["postiPrenotati"]=$dbh->getPostiPrenotatibyIDEvento($evento["IdEvento"]);
                        for ($i = 1; $i <= $evento["BigliettiDisponibili"] - $templateParams["postiPrenotati"]; ++$i) {

                            if ($i != $carrello["NumeroBiglietti"]) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            } else {
                                echo '<option value="' . $i . '" selected="selected">' . $i . '</option>';
                            }
                            if ($i == 5) {
                                $i = $evento["BigliettiDisponibili"] - $templateParams["postiPrenotati"];
                            }
                        } ?>

                    </select>
                    <p>
                    <h4>Totale: <?php echo $carrello["Totale"]; ?>€</h4>
                </section>
                
                    <button id="buttonRimuoviDaCarrello" class="btn btn-primary" onclick="rimuoviDaCarrello(<?php echo $j ?>);">Rimuovi da carrello</button>

                    <p id="IdCarrello<?php echo $j ?>" hidden><?php echo $carrello["IdCarrello"]; ?></p>
                    <p id="IdEvento<?php echo $j ?>" hidden><?php echo $carrello["IdEvento"]; ?></p>
                
        </article>
    <?php $j++; endforeach; ?>
</div>

