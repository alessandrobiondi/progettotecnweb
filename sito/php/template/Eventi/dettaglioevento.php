<?php if (count($templateParams["evento"]) == 0) : ?>
    <article>
        <p>Evento non presente</p>
    </article>
<?php
else :
    $evento = $templateParams["evento"][0];
    $nomesocieta = $templateParams["nomesocieta"][0];
    $categoria = $templateParams["categoria"][0];
    if (isUserLoggedIn()) {
        $bool = $templateParams["bool"];
        $bool2 = $templateParams["bool2"];
    }
?>
    <article class="dettaglioevento">
        <div id="container">
            <img src="<?php echo UPLOAD_DIR . $evento["ImgEvento"]; ?>" alt="" />
        </div>
        <header>
            <h1><?php echo $evento["Titolo"]; ?></h1>
        </header>
        <div id="informazioniEvento">
            <p><strong>Luogo:</strong> <?php echo $evento["Luogo"]; ?></p>
            <p><strong>Data:</strong> <?php $newDate = date("d-m-Y", strtotime($evento["Data"]));
                                        echo $newDate ?></p>
            <p><strong>Categoria:</strong> <?php echo $categoria["Descrizione"]; ?></p>
            <p><strong>Organizzatore:</strong> <?php echo $nomesocieta["NomeSocieta"]; ?></p>
            <p><strong>Posti disponibili:</strong> <?php echo $evento["BigliettiDisponibili"]; ?></p>
            <p><strong>Posti prenotati:</strong> <?php echo $templateParams["postiPrenotati"]; ?></p>
            <?php if ($evento["BigliettiDisponibili"] - $templateParams["postiPrenotati"] == 0) : ?>
                <p><strong>SOLD OUT!!!</strong> </p>
            <?php endif; ?>
        </div>
        <section>
            <div>
                <h2>Descrizione</h2>
                <p><?php echo $evento["Descrizione"]; ?></p>
            </div>
        </section>
        <div id="numerobiglietti">
            <p><strong>Prezzo:</strong> </p>
            <p id="prezzo"><?php echo $evento["Prezzo"]; ?></p>
        </div>
        <?php if (isUserLoggedIn()) : ?>
            <div>
                <?php if (($evento["BigliettiDisponibili"] - $templateParams["postiPrenotati"] > 0) && !$bool2) : ?>
                    <label for="quantità">Selezionare quantità:</label>
                    <select id="quantità">
                        <?php for ($i = 1; $i <= $evento["BigliettiDisponibili"] - $templateParams["postiPrenotati"]; ++$i) {
                            echo '<option value="' . $i . '">' . $i . '</option>';
                            if ($i == 5) {
                                $i = $evento["BigliettiDisponibili"] - $templateParams["postiPrenotati"];
                            }
                        } ?>
                    </select>
                <?php endif; ?>
            </div>
            <div><?php if ($evento["BigliettiDisponibili"] - $templateParams["postiPrenotati"] > 0) : ?>
                    <?php if (!$bool2) : ?>
                        <button id="aggiungialcarrello" type="submit" class="btn btn-primary" onclick=aggiungiAlCarrello()>Aggiungi al carrello</button>
                    <?php else : ?>
                        <button id="vaialcarrello" type="submit" class="btn btn-primary" onclick=vaiAlCarrello()>Modifica carrello</button>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <div>
                <?php if (!$bool) : ?>
                    <button id="aggiungiaipreferiti" type="submit" class="btn btn-primary" onclick=preferito()><em id="iconaPreferito" class="far fa-heart" aria-hidden="true"></em></button>
                <?php else : ?>
                    <button id="aggiungiaipreferiti" type="submit" class="btn btn-primary" onclick=preferito()><em id="iconaPreferito" class="fa fa-heart" aria-hidden="true"></em></button>
                <?php endif; ?>
            </div>
        <?php else : ?>
            <div>
                <button id="vaiallogin" type="submit" class="btn btn-primary" onclick=vaiAlLogin()>Effettua il login prima di acquistare</button>
            </div>
        <?php endif; ?>
        <p id="IdEventoCarrello" hidden><?php echo $evento["IdEvento"]; ?></p>
    </article>

<?php endif; ?>