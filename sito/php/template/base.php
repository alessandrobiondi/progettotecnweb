<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html xml:lang="it" lang="it">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $templateParams["titolo"]; ?></title>
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/categoria.js"></script>
    <script src="js/accetta.js"></script>
    <script src="js/rifiuta.js"></script>
    <script src="js/disconnetti.js"></script>
    <script src="js/evento.js"></script>
    <script src="js/gestioneEventi.js"></script>
    <script src="js/leggi.js"></script>
    <script src="js/search.js"></script>
    <script src="js/carrello.js"></script>
    <script src="js/preferiti.js"></script>
    <script src="js/utente.js"></script>
    <script src="js/utenteOrg.js"></script>
    <script src="js/gestioneIscrizione.js"></script>
    <script src="js/footer.js"></script>
    <script src="js/ritardo.js"></script>
    <script>
        $(document).on("keypress", "input", function(e) {
            if (e.which == 13) {
                cercaEvento();
            }
        });
    </script>
    <script src="js/imgName.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <!-- cdn for modernizr, if you haven't included it already -->
    <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
    <!-- polyfiller file to detect and load polyfills -->
    <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
    <script>
        webshims.setOptions('waitReady', false);
        webshims.setOptions('forms-ext', {
            types: 'date'
        });
        webshims.polyfill('forms forms-ext');
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />

    <?php
    if (isset($templateParams["js"])) :
        foreach ($templateParams["js"] as $script) :
    ?>
            <script src="<?php echo $script; ?>"></script>
    <?php
        endforeach;
    endif;
    ?>
</head>

<body id=<?php echo $templateParams["titolo"] ?>>
    <header>
        <div id="mySidenav" class="sidenav">

            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <a <?php isActive("index.php"); ?> href="index.php">Home</a>

            <a <?php isActive("login.php"); ?> href="login.php">Login</a>
            <?php if (!isOrganizerUserLoggedIn()) : ?>
                <a id="sep">CATEGORIE</a>
                <?php
                $cat = $dbh->getCategorie();
                foreach ($cat as $categoria) :
                    $id = $categoria["IdCategoria"]; ?>
                    <a href="index.php?IdCategoria=<?php echo $id ?>"><?php echo $categoria["Descrizione"] ?></a>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "100%";
            }

            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
        </script>
        <div>
            <span id="menuIcon" onclick="openNav()" class="fas fa-bars"></span>
            <span id="titolo">
                Scalper
            </span>
            <a <?php isActive("login.php"); ?> href="login.php"><span id="utenteIcon" class="fas fa-user-circle"></span></a>
            <?php if (isOrganizerUserLoggedIn()) : ?>
                <a <?php isActive("creaevento.php"); ?> href="creaevento.php"><span id="creaIcon" class="fa fa-plus"></span></a>
            <?php elseif (isAdminUserLoggedIn()) : ?>
            <?php else : ?>
                <?php if (isUserLoggedIn()) : ?>
                    <?php if (isset($templateParams["carrello"])  && count($templateParams["carrello"]) != 0) : ?>
                        <a <?php isActive("visualizzaCarrello.php"); ?> href="visualizzaCarrello.php"><span id="carrelloIcon" class="fas fa-shopping-cart"></span></a>
                        <?php echo "<span id='badgeCarrello' class='badge badge-notify'>" . count($templateParams['carrello']) . "</span>"; ?>
                    <?php else : ?>
                        <a <?php isActive("visualizzaCarrello.php"); ?> href="visualizzaCarrello.php"><span id="carrelloIcon" class="fas fa-shopping-cart"></span></a>
                        <?php echo "<span id='badgeCarrello' class='badge badge-notify' style='display: none'>0</span>"; ?>
                    <?php endif; ?>
                <?php else : ?>
                    <a <?php isActive("login.php"); ?> href="login.php"><span id="carrelloIcon" class="fas fa-shopping-cart"></span></a>
                    <?php echo "<span id='badgeCarrello' class='badge badge-notify' style='display: none'>0</span>"; ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php if (isset($templateParams["notificheNONlette"])) : ?>
                <?php if ($templateParams["notificheNONlette"] > 0) : ?>
                    <a <?php isActive("notifiche.php"); ?> href="notifiche.php"><span id="notificaIcon" class="fa fa-bell"></span></a>
                    <?php echo "<span id='badgeNotifica' class='badge badge-notify'>" . $templateParams['notificheNONlette'] . "</span>"; ?>
                <?php else : ?>
                    <a <?php isActive("notifiche.php"); ?> href="notifiche.php"><span id="notificaIcon" class="far fa-bell"></span></a>
                <?php endif; ?>
            <?php else : ?>
                <a <?php isActive("notifiche.php"); ?> href="notifiche.php"><span id="notificaIcon" class="far fa-bell"></span></a>
            <?php endif; ?>
        </div>
        <?php if (!isOrganizerUserLoggedIn()) : ?>
            <div class="search">
                <button type="submit" class="searchButton" onclick="cercaEvento()">
                    <em class="fa fa-search"></em>
                </button>
                <input type="text" title="searchBar" class="searchTerm" id="searchbar" placeholder="Cerchi altro?">
            </div>
        <?php endif; ?>
    </header>
    <main>
        <?php
        if (isset($templateParams["nome"])) {
            require($templateParams["nome"]);
        }
        ?>
    </main>
    <footer id="footer">Roberto Di Lena Matr:825578 - Andrea Fabbri Matr:825610 - Alessandro Biondi Matr:825826</footer>
</body>

</html>