<?php
require_once("bootstrap.php");

$nome = $_POST["inputNome"];
$cognome = $_POST["inputCognome"];
$mail = $_POST["inputMail"];
$psw =$_POST["inputPassword"];
$address =$_POST["inputIndirizzo"];
$city = $_POST["inputCittà"];
$CAP =$_POST["inputCAP"];

$registrationRes=0;

$existingUser=$dbh->checkUser($mail);
if(count($existingUser)==0){
    $registrationRes= $dbh->registerUser($nome,$cognome,$mail,$psw,$address,$city,$CAP);
}
else{
    $templateParams["titolo"] = "Scalper-Fail";
    $templateParams["nome"] = "Login-Registrazione/utente-esistente.php";
}
  
if($registrationRes){
    $login_result = $dbh->checkLogin($mail, $psw);
    if(count($login_result)!=0){ 
        registerLoggedUser($login_result[0]);
    }

    if(isUserLoggedIn()){
        $templateParams["titolo"] = "Scalper-User";
        $templateParams["nome"] = "Login-Registrazione/user-profile.php";
    }
}

require("template/base.php");
?>