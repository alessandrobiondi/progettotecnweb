function aggiungiAlCarrello() {
    let n = document.getElementById('quantità').value;
    let prezzo = document.getElementById('prezzo').innerHTML;
    let id = document.getElementById('IdEventoCarrello').innerHTML;
    let tot = n * prezzo;
    $.post("aggiungiACarrello.php", { N: n, Tot: tot, ID: id });
    incrementaIcona();
    sleep(100);
    window.location.replace("visualizzaCarrello.php");
}

function vaiAlCarrello(){
    window.location.replace("visualizzaCarrello.php");
}

function decrementaIcona() {
    let badge = document.getElementById("badgeCarrello");
    let n = badge.textContent;
    n--;
    if (n != 0) {
        badge.textContent = n;
    } else {
        badge.style.display = "none";
    }
}

function incrementaIcona() {
    let badge = document.getElementById("badgeCarrello");
    let n = badge.textContent;
    n++;
    if (n == 1) {
        badge.style.display = "";
    }
    badge.textContent = n;
}

function rimuoviDaCarrello(i){
    let id = document.getElementById("IdCarrello"+i).innerHTML;
    $.post("rimuoviDaCarrello.php", {ID: id });
    window.location.reload();
    decrementaIcona();
}
function modificaQuantità(i){
    let e = document.getElementById("quantità"+i);
    let n = e.options[e.selectedIndex].value;
    let prezzo = document.getElementById("prezzoBiglietto"+i).innerHTML;
    let tot = n * prezzo;
    let id = document.getElementById("IdCarrello"+i).innerHTML;
    $.post("modificaQuantità.php", { N: n, Tot: tot, ID: id });
    window.location.reload();
}

function procediConLOrdine(){
    window.location.replace("pagamento.php");
}

function paga(){
    $.post("acquistoAvvenuto.php");
    window.location.replace("acquistoAvvenuto.php");
    window.location.reload();
}