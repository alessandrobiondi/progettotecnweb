<?php
    require_once("bootstrap.php");
    $oldPsw=$_POST["inputOldPsw"];
    $psw=$_POST["inputPsw"];
    $psw2=$_POST["inputNPsw"];

    if($psw==$psw2){
        if(isset($_SESSION["IdUtente"])){
            if($_SESSION["Password"]==$oldPsw){
                $dbh->updatePassword($psw2,$_SESSION["IdUtente"]);
                $_SESSION["PSW"]=2; //Ok
            }else{
                $_SESSION["PSW"]=3; //Password vecchia sbagliata
            }
        }elseif(isset($_SESSION["IdOrganizzatore"])){
            if($_SESSION["Password"]==$oldPsw){
                $dbh->updatePasswordOrg($psw2,$_SESSION["IdOrganizzatore"]);
                $_SESSION["PSW"]=2; //Ok
            }else{
                $_SESSION["PSW"]=3; //Password vecchia sbagliata
            }
        }else{
            if($_SESSION["Password"]==$oldPsw){
                $dbh->updatePasswordAmm($psw2,$_SESSION["IdAmministratore"]);
                $_SESSION["PSW"]=2; //Ok
            }else{
                $_SESSION["PSW"]=3; //Password vecchia sbagliata
            }
        }
    }else{
        $_SESSION["PSW"]=1; //Psw nuove non corrispondenti
    }
    header("Location:modificaPassword.php");
?>