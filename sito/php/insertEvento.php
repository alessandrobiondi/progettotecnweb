<?php
require_once("bootstrap.php");

list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["ImgEvento"]);
$titolo = $_POST["inputNomeEvento"];
$luogo = $_POST["inputLuogo"];
$data = $_POST["inputData"];
$categoria = $_POST["inputCategoria"];
$descrizione = $_POST["inputDescrizione"];
$anteprima = $_POST["inputAnteprima"];
$posti = $_POST["inputPosti"];
$prezzo = $_POST["inputPrezzo"];
$id = $_SESSION["IdOrganizzatore"];

if (strlen($anteprima) > 200) {
    $anteprima = substr($anteprima, 0, 200);
}
$imgarticolo = $msg;    
$dbh->creaEvento($titolo, $luogo, $data, $categoria + 1, $descrizione, $posti, $prezzo, $id, $imgarticolo, $anteprima);
header("location: index.php");
?>

