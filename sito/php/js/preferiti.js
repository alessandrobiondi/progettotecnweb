function preferito() {
    let id = document.getElementById('IdEventoCarrello').innerHTML;
    if (document.getElementById("iconaPreferito").className.match(/(?:^|\s)far fa-heart(?!\S)/)) { //aggiungo preferito
        $.post("aggiungiPreferito.php", { IdEvento: id });
        document.getElementById("iconaPreferito").className = "fa fa-heart";
    }
    //elimino preferito
    else {
        $.post("eliminaPreferito.php", { IdEvento: id });
        document.getElementById("iconaPreferito").className = "far fa-heart";
    }
}