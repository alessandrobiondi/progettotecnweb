<?php
require_once("bootstrap.php");

$templateParams["elencocategorie"] = $dbh->getCategorie();
if (isUserLoggedIn()) {
    $templateParams["titolo"] = "Scalper-Acquisto";
    $templateParams["nome"] = "Carrello/acquistoAvvenuto.php";
    $templateParams["carrello"] = $dbh->getCarrelloByUtente($_SESSION["IdUtente"]);
    if (isset($_SESSION["IdUtente"]) ) {
        foreach ($templateParams["carrello"] as $carrello) :
            $date=date('Y-m-d');
            $dbh->insertOrdine($carrello["Totale"], $date,$carrello["NumeroBiglietti"],$_SESSION["IdUtente"],$carrello["IdEvento"]);
            
            $idOrdine=$dbh->getLastOrdine();
            $dbh->insertComprende($carrello["IdEvento"],$idOrdine[0]["id"]);
            $dbh->removeCarrello($carrello["IdCarrello"]);
        endforeach;
    }
}
require("template/base.php");
?>
