<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "Scalper-Login";
$templateParams["nome"] = "Login-Registrazione/login-form.php";

if(isUserLoggedIn()){
    $templateParams["titolo"] = "Scalper-User";
    $templateParams["nome"] = "Login-Registrazione/user-profile.php";
    $templateParams["notifiche"] = $dbh->getNotificheByIdUtente($_SESSION["IdUtente"]);
    $i = 0;
    foreach ($templateParams["notifiche"] as $notifica) {
        if ($notifica["Letto"] == 0) {
            $i++;
        }
    }
    $templateParams["notificheNONlette"] = $i;
    $templateParams["carrello"] = $dbh->getCarrelloByUtente($_SESSION["IdUtente"]);
}else{
    if(isset($_POST["mail"]) && isset($_POST["password"])){
        $login_result = $dbh->checkLogin($_POST["mail"], $_POST["password"]);
        if(count($login_result)==0){                                                                            /*Se login standard non ok provo login organizzatore*/
            $loginOrganizzatore_result = $dbh->checkLoginOrganizzatore($_POST["mail"], $_POST["password"]);
            if(count($loginOrganizzatore_result)==0){                                                           /*Se login organizzatore non ok provo login admin*/
                $loginAdmin_result=$dbh->checkLoginAdmin($_POST["mail"], $_POST["password"]);
                if(count($loginAdmin_result)==0){                                                               /*Se login admin non ok dati non corretti*/
                    $templateParams["errorelogin"] = "Errore! Username o password non corretti";
                }
                else{
                    registerLoggedAdminUser($loginAdmin_result[0]);
                }   
            }else{
                if($loginOrganizzatore_result[0]["Accettato"]==0){                                              /*Check organizzatore accettato o meno*/
                    $templateParams["titolo"] = "Scalper-Organizer";
                    $templateParams["nome"] = "Login-Registrazione/accettazione.php";
                }else{
                    registerLoggedOrganizerUser($loginOrganizzatore_result[0]);
                }
            }
        }
        else{
            registerLoggedUser($login_result[0]);
        }
    }
    
    if(isUserLoggedIn()){
        $templateParams["titolo"] = "Scalper-User";
        $templateParams["nome"] = "Login-Registrazione/user-profile.php";
        $templateParams["notifiche"] = $dbh->getNotificheByIdUtente($_SESSION["IdUtente"]);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
        $templateParams["carrello"] = $dbh->getCarrelloByUtente($_SESSION["IdUtente"]);
    }
    else if(isOrganizerUserLoggedIn()){
        $templateParams["titolo"] = "Scalper-Organizer";
        $templateParams["nome"] = "Login-Registrazione/loginOrganizer-home.php";
        $eventiOrg=$dbh->getEventsByIdOrganizer($_SESSION["IdOrganizzatore"]);
        foreach($eventiOrg as $evento){
            $postiprenotati=$dbh->getPostiPrenotatibyIDEvento($evento["IdEvento"]);
            $eventisoldout=$dbh->getSoldOutEvents($_SESSION["IdOrganizzatore"],$postiprenotati);
            if(count($eventisoldout)>0){
                foreach($eventisoldout as $evento){
                    $testo="L'evento ". $evento["Titolo"]." è soldout";
                    $dbh->notificaOrganizzatore($testo,$_SESSION["IdOrganizzatore"]);
                    $dbh->eventoNotificato($evento["IdEvento"]);
                }
            }
        }   
        $templateParams["notifiche"]=$dbh->getNotificheByIdOrganizzatore($_SESSION["IdOrganizzatore"]);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }else if(isAdminUserLoggedIn()){
        $templateParams["titolo"] = "Scalper-Admin";
        $templateParams["nome"] = "adminHome.php";
        $templateParams["notifiche"]=$dbh->getNotificheByIdAmministratore($_SESSION["IdAmministratore"]);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }
}
$_SESSION["PSW"]=0;
require("template/base.php");
?>