<?php
require_once("bootstrap.php");
$templateParams["titolo"] = "Scalper-Carrello-Vuoto";

$templateParams["nome"] = "Carrello/carrello.php";
if(isUserLoggedIn()){
    if (isset($_SESSION["IdUtente"])) {
        $id = $_SESSION["IdUtente"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdUtente($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }
    $templateParams["carrello"] = $dbh->getCarrelloByUtente($_SESSION["IdUtente"]);
    $templateParams["Totale"] = $dbh->getTotal($_SESSION["IdUtente"]);
    if(count($templateParams["carrello"])==0){
        
        $templateParams["nome"] = "Carrello/carrelloVuoto.php";
    }
    else{
        $templateParams["titolo"] = "Scalper-Carrello";
    }
}else{
    $templateParams["nome"] = "Carrello/carrelloInaccessibile.php";
}

require("template/base.php");
?>