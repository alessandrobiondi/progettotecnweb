<?php

class DatabaseHelper{
    private $db;
    private $accettato=0;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connesione fallita al db");
        }
    }

    public function checkLogin($username, $password){
        $stmt = $this->db->prepare("SELECT Nome,Mail,IdUtente,Password FROM utente WHERE Mail = ? AND Password = ?");
        $stmt->bind_param("ss", $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkLoginOrganizzatore($username, $password){
        $stmt = $this->db->prepare("SELECT Nome,Mail,IdOrganizzatore,Accettato,Password FROM utente_organizzatore WHERE Mail = ? AND Password = ?");
        $stmt->bind_param("ss", $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    
    public function checkLoginAdmin($username, $password){
        $stmt = $this->db->prepare("SELECT IdAmministratore,Nome,Mail,Password FROM utente_amministratore WHERE Mail = ? AND Password = ?");
        $stmt->bind_param("ss", $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function registerUser($nome, $cognome, $mail, $password, $indirizzo, $citta, $CAP){
        $stmt =$this->db->prepare("INSERT INTO utente (Nome, Cognome, Indirizzo, Citta, CAP, Mail, Password ) VALUES(?,?,?,?,?,?,?)"); //Inserimento valori
        $stmt->bind_param('ssssiss',$nome, $cognome,$indirizzo, $citta,$CAP, $mail, $password);
        $result=$stmt->execute();
        return $result;
    }
    
    public function registerOrganizerUser($nome, $cognome, $mail, $password, $indirizzo, $citta, $CAP, $nomeSocieta,$accettato){
        $stmt =$this->db->prepare("INSERT INTO utente_organizzatore (Nome, Cognome, Indirizzo, Citta, CAP, NomeSocieta, Mail, Password,Accettato ) VALUES(?,?,?,?,?,?,?,?,?)"); //Inserimento valori
        $stmt->bind_param('ssssisssi',$nome, $cognome,$indirizzo, $citta,$CAP, $nomeSocieta, $mail, $password,$accettato);
        $result=$stmt->execute();
        return $result;
    }

    public function checkUser($mail){
        $stmt=$this->db->prepare("SELECT Mail FROM utente WHERE mail=?");
        $stmt->bind_param('s', $mail);
        $res=$stmt->execute();
        $res=$stmt->get_result();
        return $res->fetch_all(MYSQLI_ASSOC);
    }

    public function checkOrganizerUser($mail){
        $stmt=$this->db->prepare("SELECT Mail FROM utente_organizzatore WHERE mail=?");
        $stmt->bind_param('s', $mail);
        $res=$stmt->execute();
        $res=$stmt->get_result();
        return $res->fetch_all(MYSQLI_ASSOC);
    }

    public function updatePassword($password,$id){
        $stmt =$this->db->prepare("UPDATE utente SET Password=? WHERE IdUtente=?");
        $stmt->bind_param('si',$password, $id);
        $result=$stmt->execute();
        return $result;
    }

    public function updatePasswordOrg($password,$id){
        $stmt =$this->db->prepare("UPDATE utente_organizzatore SET Password=? WHERE IdOrganizzatore=?");
        $stmt->bind_param('si',$password, $id);
        $result=$stmt->execute();
        return $result;
    }

    public function updatePasswordAmm($password,$id){
        $stmt =$this->db->prepare("UPDATE utente_amministratore SET Password=? WHERE IdAmministratore=?");
        $stmt->bind_param('si',$password, $id);
        $result=$stmt->execute();
        return $result;
    }

    public function getEventobyID($id){
        $stmt = $this->db->prepare("SELECT * FROM evento WHERE IdEvento = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotAcceptedOrg(){
        $stmt = $this->db->prepare("SELECT Nome,Cognome, IdOrganizzatore, NomeSocieta, Mail FROM utente_organizzatore WHERE Accettato=0");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function acceptOrg($id){
        $stmt = $this->db->prepare("UPDATE utente_organizzatore SET Accettato=1 WHERE IdOrganizzatore= ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
    }

    public function declineOrg($id){
        $stmt = $this->db->prepare("DELETE FROM utente_organizzatore WHERE  IdOrganizzatore= ? AND Accettato=0");
        $stmt->bind_param("i", $id);
        $stmt->execute();
    }

    public function getNomeSocietabyID($id){
        $stmt = $this->db->prepare("SELECT NomeSocieta FROM utente_organizzatore WHERE IdOrganizzatore=?");
        $stmt->bind_param("i",$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDescrizioneCategoriabyID($id)
    {
        $stmt = $this->db->prepare("SELECT Descrizione FROM categoria WHERE IdCategoria=?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategorie()
    {
        $stmt = $this->db->prepare("SELECT IdCategoria, Descrizione FROM categoria");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPostiPrenotatibyIDEvento($id)
    {
        $stmt = $this->db->prepare("SELECT IdOrdine FROM comprende WHERE IdEvento=?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $listaordini = $result->fetch_all(MYSQLI_ASSOC);

        $numeroBigliettiPrenotati=0;
        for ($i = 0; $i < count($listaordini); ++$i) {
            $ordine = $listaordini[$i];

            $stmt = $this->db->prepare("SELECT NumeroBiglietti FROM ordine WHERE IdOrdine=?");
            $stmt->bind_param("i", $ordine['IdOrdine']);
            $stmt->execute();
            $res = $stmt->get_result();
            $result = $res->fetch_all(MYSQLI_ASSOC);
            $numero = $result[0];
            $numeroBigliettiPrenotati+=$numero['NumeroBiglietti'];
        }
        return $numeroBigliettiPrenotati;
    }

    public function newCategoria($desc){
        $stmt = $this->db->prepare("INSERT INTO categoria (Descrizione) VALUES (?)");
        $stmt->bind_param("s", $desc);
        $stmt->execute();
    }

    public function creaEvento($nome, $luogo, $data, $categoria, $descrizione, $posti, $prezzo, $idorganizzatore, $img, $anteprima)
    {
        $stmt = $this->db->prepare("INSERT INTO `evento`(`Titolo`, `Descrizione`,`Anteprima`, `Data`, `Luogo`, `BigliettiDisponibili`, `Prezzo`, `IdOrganizzatore`, `IdCategoria`, `ImgEvento`) VALUES (?,?,?,?,?,?,?,?,?,?)"); //Inserimento valori
        $stmt->bind_param('sssssidiis', $nome, $descrizione, $anteprima, $data, $luogo, $posti, $prezzo, $idorganizzatore, $categoria, $img);
        $stmt->execute();
    }
    public function modificaEvento($nome, $luogo, $data, $categoria, $descrizione, $evento, $img, $anteprima)
    {
        $stmt = $this->db->prepare("UPDATE `evento`SET Titolo=?, Descrizione=?,Anteprima=?, Data=?,Luogo=?,IdCategoria=?,ImgEvento=? WHERE IdEvento=?"); //Inserimento valori
        $stmt->bind_param('sssssisi', $nome, $descrizione, $anteprima, $data, $luogo, $categoria, $img, $evento);
        $stmt->execute();
    }
    public function modificaEventoNoImg($nome, $luogo, $data, $categoria, $descrizione, $evento,$anteprima)
    {
        $stmt = $this->db->prepare("UPDATE `evento`SET Titolo=?, Descrizione=?,Anteprima=?, Data=?,Luogo=?,IdCategoria=? WHERE IdEvento=?"); //Inserimento valori
        $stmt->bind_param('sssssii', $nome, $descrizione,$anteprima, $data, $luogo, $categoria, $evento);
        $stmt->execute();
    }

    public function getNotificheByIdAmministratore($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM notifiche_amministratore WHERE IdAmministratore=? ORDER BY Letto, IdNotifica DESC"); //Inserimento valori
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function leggiAmministratore($id){
        $stmt = $this->db->prepare("UPDATE notifiche_amministratore SET Letto=1 WHERE IdNotifica= ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
    }

    public function notificaAmministratore($messaggio, $id){
        $stmt = $this->db->prepare("INSERT notifiche_amministratore(`Messaggio`, `IdAmministratore`) VALUE (?,?)");
        $stmt->bind_param("si", $messaggio, $id);
        $stmt->execute();
    }

    public function getNotificheByIdOrganizzatore($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM notifiche_organizzatore WHERE IdOrganizzatore=? ORDER BY Letto, IdNotifica DESC"); //Inserimento valori
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function leggiOrganizzatore($id){
        $stmt = $this->db->prepare("UPDATE notifiche_organizzatore SET Letto=1 WHERE IdNotifica= ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
    }

    public function notificaOrganizzatore($messaggio, $id){
        $stmt = $this->db->prepare("INSERT notifiche_organizzatore(`Messaggio`, `IdOrganizzatore`) VALUE (?,?)");
        $stmt->bind_param("si", $messaggio, $id);
        $stmt->execute();
    }

    public function getNotificheByIdUtente($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM notifiche_utente WHERE IdUtente=? ORDER BY Letto, IdNotifica DESC"); //Inserimento valori
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function leggiUtente($id){
        $stmt = $this->db->prepare("UPDATE notifiche_utente SET Letto=1 WHERE IdNotifica= ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
    }

    public function notificaUtente($messaggio, $id){
        $stmt = $this->db->prepare("INSERT notifiche_utente(`Messaggio`, `IdUtente`) VALUE (?,?)");
        $stmt->bind_param("si", $messaggio, $id);
        $stmt->execute();
    }
    
    public function getUtenteByEventoAcquistato($id){
        $stmt = $this->db->prepare("SELECT IdUtente FROM ordine,comprende WHERE ordine.IdOrdine=comprende.IdOrdine AND comprende.IdEvento=?");
        $stmt->bind_param("i",$id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEvents($n){
        $query = "SELECT IdEvento, Titolo, ImgEvento, Data, Descrizione, Anteprima  FROM evento WHERE BigliettiDisponibili > 0 ORDER BY Data DESC";
        $query = $query . " LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllEvents(){
        $query = "SELECT IdEvento, Titolo, ImgEvento, Data, Descrizione, Anteprima  FROM evento WHERE BigliettiDisponibili > 0 ORDER BY Data ASC";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSoldOutEvents($id,$postiprenotati){
        $query = "SELECT IdEvento, Titolo, ImgEvento, Data, Descrizione  FROM evento WHERE IdOrganizzatore=? AND (BigliettiDisponibili-$postiprenotati) = 0 AND Notificato=0";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function eventoNotificato($id){
        $query="UPDATE evento SET Notificato=1 WHERE IdEvento=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $id);
        $stmt->execute();
    }

    public function getOrdiniByIdUtente($id){
        $query = "SELECT Totale ,DataAcquisto, NumeroBiglietti, Data, Titolo FROM ordine, evento, comprende WHERE comprende.IdOrdine=ordine.IdOrdine AND comprende.IdEvento=evento.IdEvento AND IdUtente=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function insertOrdine($totale,$dataAcquisto,$nBiglietti,$idUtente,$idEvento){
        $query = "INSERT INTO ordine(Totale, DataAcquisto, NumeroBiglietti, IdUtente) VALUES (?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("dsii", $totale,$dataAcquisto,$nBiglietti,$idUtente);
        $stmt->execute(); 
    }
    public function getLastOrdine(){
        $query="SELECT max(IdOrdine) as id FROM ordine";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertComprende($idEvento,$idOrdine){
        $query = "INSERT INTO comprende (IdEvento, IdOrdine) VALUES (?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii", $idEvento,$idOrdine);    
        $stmt->execute();  
    }
    
    public function getEventsByCategory($n){
        $query = "SELECT IdEvento, Titolo, ImgEvento, Data, Descrizione, IdCategoria, Anteprima FROM evento WHERE IdCategoria=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByFilter($filter){
        $param = "%{$filter}%";
        $stmt = $this->db->prepare("SELECT IdEvento, Titolo, ImgEvento, Data, Descrizione, IdCategoria, Anteprima FROM evento WHERE Titolo LIKE ?");
        $stmt->bind_param('s', $param);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByIdOrganizer($n){
        $query = "SELECT IdEvento, Titolo, ImgEvento, Data, Descrizione, IdCategoria, Anteprima FROM evento WHERE IdOrganizzatore=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getCarrelloByUtente($idUtente){
        $query = "SELECT * FROM carrello WHERE IdUtente=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idUtente);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function isEventOnCart($idEvento,$idUtente){
        $b = 0;
        $query="SELECT * FROM carrello WHERE IdEvento=? AND IdUtente=?";
        $stmt = $this->db->prepare($query);    
        $stmt->bind_param("ii",$idEvento,$idUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        if(count($result->fetch_all(MYSQLI_ASSOC))>0){
            $b=1;
        }
        return $b;

    }
    public function insertCarrello($idEvento,$idUtente,$nBiglietti,$totale){
        $query = "INSERT INTO `carrello`(`IdEvento`, `IdUtente`, `NumeroBiglietti`, `Totale`) VALUES (?,?,?,?)";
        $stmt = $this->db->prepare($query);      
        $stmt->bind_param("iiid",$idEvento,$idUtente,$nBiglietti,$totale);
        $stmt->execute();
    }

    public function updateCarrello($idEvento,$idUtente,$nBiglietti,$totale){
        $query="UPDATE carrello SET NumeroBiglietti=?, Totale=? WHERE IdEvento=? AND IdUtente=?";
        $stmt = $this->db->prepare($query); 
        $stmt->bind_param("idii", $nBiglietti ,$totale,$idEvento,$idUtente);  
        $stmt->execute();
    }

    public function removeCarrello($idCarrello){
        $query = "DELETE FROM `carrello` WHERE IdCarrello=?";
        $stmt = $this->db->prepare($query);      
        $stmt->bind_param("i",$idCarrello);
        $stmt->execute();
    }

    public function insertPreferito($idEvento,$idUtente){
        $query = "INSERT INTO `preferiti`(`IdEvento`, `IdUtente`) VALUES (?,?)";
        $stmt = $this->db->prepare($query);      
        $stmt->bind_param("ii",$idEvento,$idUtente);
        $stmt->execute();
    }

    public function deletePreferito($idEvento,$idUtente){
        $query = "DELETE FROM `preferiti` WHERE IdEvento=? AND IdUtente=?";
        $stmt = $this->db->prepare($query);      
        $stmt->bind_param("ii",$idEvento,$idUtente);
        $stmt->execute();
    }

    public function getPreferitiByIdUtente($idUtente){
        $query = "SELECT * FROM preferiti WHERE IdUtente=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idUtente);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOrganizzatoreByID($idOrganizzatore){
        $query = "SELECT * FROM utente_organizzatore WHERE IdOrganizzatore=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idOrganizzatore);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTotal($IdUtente) {
        $query = "SELECT SUM(Totale) FROM carrello WHERE IdUtente=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $IdUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function modificaQuantità($ID,$N,$Tot){
        $query="UPDATE carrello SET NumeroBiglietti=?, Totale=? WHERE IdCarrello=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("idi", $N, $Tot, $ID);
        $stmt->execute();
    }

    public function getNTickOnCart($idEvento,$idUtente){
        $query="SELECT NumeroBiglietti FROM carrello WHERE IdEvento=? AND IdUtente=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii", $idEvento,$idUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

}
?>