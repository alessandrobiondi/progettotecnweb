<?php
require_once("bootstrap.php");
list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["ImgEvento"]);
$titolo = $_POST["inputNomeEvento"];
$luogo = $_POST["inputLuogo"];
$data = $_POST["inputData"];
$categoria = $_POST["inputCategoria"];
$descrizione = $_POST["inputDescrizione"];
$anteprima = $_POST["inputAnteprima"];
$idevento = $_POST["id"];

if (strlen($anteprima) > 200) {
    $anteprima = substr($anteprima, 0, 200);
}
if ($result != 0) {
    $imgarticolo = $msg;
    $dbh->modificaEvento($titolo, $luogo, $data, $categoria + 1, $descrizione, $idevento, $imgarticolo, $anteprima);
    $utentiDaNotificare = $dbh->getUtenteByEventoAcquistato($idevento);
    foreach ($utentiDaNotificare as $user) {
        $dbh->notificaUtente("Le informazioni dell'evento " . $titolo . " sono state modificate, la invitiamo a ricontrollare", $user["IdUtente"]);
    }
    header("location: index.php");
} else {
    $dbh->modificaEventoNoImg($titolo, $luogo, $data, $categoria + 1, $descrizione, $idevento, $anteprima);
    $utentiDaNotificare = $dbh->getUtenteByEventoAcquistato($idevento);
    foreach ($utentiDaNotificare as $user) {
        $dbh->notificaUtente("Le informazioni dell'evento " . $titolo . " sono state modificate, la invitiamo a ricontrollare", $user["IdUtente"]);
    }
    header("location: index.php");
}
?>
