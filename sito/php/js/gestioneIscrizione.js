function checkNewUser(ev) {
    let nome = document.getElementById("inputNome").value;
    let cognome = document.getElementById("inputCognome").value;
    let mail = document.getElementById("inputMail").value;
    let psw = document.getElementById("inputPassword").value;
    let address = document.getElementById("inputIndirizzo").value;
    let city = document.getElementById("inputCittà").value;
    let CAP = document.getElementById("inputCAP").value;

    if (nome != "" && cognome != "" && mail != "" && mail != "" && psw != "" && address != "" && city != "" && CAP != "") {
        return true;
    }
    else {
        if (nome == "") {
            document.getElementById("inputNome").style.border = "1px solid red";
        }
        if (cognome == "") {
            document.getElementById("inputCognome").style.border = "1px solid red";
        }
        if (mail == "") {
            document.getElementById("inputMail").style.border = "1px solid red";
        }
        if (psw == "") {
            document.getElementById("inputPassword").style.border = "1px solid red";
        }
        if (address == "") {
            document.getElementById("inputIndirizzo").style.border = "1px solid red";
        }
        if (city == "") {
            document.getElementById("inputCittà").style.border = "1px solid red";
        }
        if (CAP == "") {
            document.getElementById("inputCAP").style.border = "1px solid red";
        }
        return false;
    }
}

function checkNewOrgUser(ev) {
    let nome = document.getElementById("inputNome").value;
    let cognome = document.getElementById("inputCognome").value;
    let mail = document.getElementById("inputMail").value;
    let psw = document.getElementById("inputPassword").value;
    let address = document.getElementById("inputAddress").value;
    let city = document.getElementById("inputCittà").value;
    let CAP = document.getElementById("inputCAP").value;
    let società = document.getElementById("inputSocieta").value;

    if (nome != "" && cognome != "" && mail != "" && mail != "" && psw != "" && address != "" && city != "" && CAP != "" && società!="") {
        return true;
    }
    else {
        if (nome == "") {
            document.getElementById("inputNome").style.border = "1px solid red";
        }
        if (cognome == "") {
            document.getElementById("inputCognome").style.border = "1px solid red";
        }
        if (mail == "") {
            document.getElementById("inputMail").style.border = "1px solid red";
        }
        if (psw == "") {
            document.getElementById("inputPassword").style.border = "1px solid red";
        }
        if (address == "") {
            document.getElementById("inputAddress").style.border = "1px solid red";
        }
        if (city == "") {
            document.getElementById("inputCittà").style.border = "1px solid red";
        }
        if (CAP == "") {
            document.getElementById("inputCAP").style.border = "1px solid red";
        }
        if (società == "") {
            document.getElementById("inputSocieta").style.border = "1px solid red";
        }
        return false;
    }
}