<?php if (isset($templateParams["titolo_pagina"])) : ?>
    <h2><?php echo $templateParams["titolo_pagina"]; ?></h2>
<?php endif; ?>
<ul id="eventList">
    <?php foreach ($templateParams["eventi"] as $evento) : ?>
        <li>
            <a href="dettaglioevento.php?id=<?php echo $evento["IdEvento"]; ?>">
                <article class="evento">
                    <header>
                        <h2><?php echo $evento["Titolo"]; ?></h2>
                    </header>

                    <div class="imgEvento">
                        <img alt="" src="<?php echo UPLOAD_DIR . $evento["ImgEvento"]; ?>"  />
                    </div>

                    <div class="descrizioneEvento">
                        
                        <section>
                            <h3>
                                <?php $newDate = date("d-m-Y", strtotime($evento["Data"]));
                                echo $newDate ?>
                            </h3>
                            <p>
                                <?php echo $evento["Anteprima"]; ?>
                            </p>
                            
                        </section>
                    </div>
                </article>
            </a>
        </li>
    <?php endforeach; ?>
</ul>