        <form action="registrazioneOrganizzatore.php" method="post" enctype="multipart/form-data" onsubmit="return checkNewOrgUser()">
            <div class="form-row">
            <div class="form-group col-md-3">
                    <label for="inputNome">Nome</label>
                    <input type="text" id="inputNome" class="form-control" name="inputNome" placeholder="Nome">
                </div>
                <div class="form-group col-md-3">
                    <label for="inputCognome">Cognome</label>
                    <input type="text" id="inputCognome" class="form-control" name="inputCognome" placeholder="Cognome">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputSocieta">Societa'</label>
                    <input type="text" id="inputSocieta" class="form-control" name="inputSocieta" placeholder="Societa'">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputMail">Email</label>
                    <input type="email" id="inputMail" class="form-control" name="inputMail" placeholder="Email">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword">Password</label>
                    <input type="password" id="inputPassword" class="form-control" name="inputPassword" placeholder="Password">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputAddress">Indirizzo</label>
                    <input type="text" id="inputAddress" class="form-control" name="inputAddress" placeholder="Indirizzo">
                </div>
                <div class="form-group col-md-3">
                    <label for="inputCittà">Citta'</label>
                    <input type="text" id="inputCittà" class="form-control" name="inputCittà" placeholder="Città">
                </div>
                <div class="form-group col-md-3">
                    <label for="inputCAP">CAP</label>
                    <input type="text" id="inputCAP" class="form-control" name="inputCAP" placeholder="CAP">
                </div>
            </div>
            <input type="submit" name= "submit" class="btn btn-primary" value="Registrazione">
        </form>