<form id="formpagamento">
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="inputNome">Nome e Cognome</label>
            <input type="text" class="form-control" id="inputNome" name="inputNome" placeholder="Nome e Cognome">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="inputNumero">Numero Carta</label>
            <input type="text" class="form-control" id="inputNumero" name="inputNumero"  placeholder="Numero carta">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="inputScadenza">Scadenza</label>
            <input type="date" class="form-control" name="inputScadenza" id="inputScadenza" >
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="inputCvv">CVV</label>
            <input type="text" class="form-control" name="inputCvv" id="inputCvv" placeholder="CVV">
        </div>
    </div>
    <div>
        <p id="totale">Totale: <?php echo $templateParams["Totale"][0]["SUM(Totale)"]; ?>€</p>
    </div>
    <div>
        <button class="btn btn-primary" onclick=paga()>Paga ora</button>
    </div>

</form>