<?php
require_once("bootstrap.php");

if (isUserLoggedIn()) {
    $templateParams["titolo"] = "Scalper-Notifiche";
    $templateParams["nome"] = "Notifiche/notificheUtente.php";
    $idevento = -1;
    if (isset($_SESSION["IdUtente"])) {
        $id = $_SESSION["IdUtente"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdUtente($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
        $templateParams["carrello"] = $dbh->getCarrelloByUtente($id);
    }
} elseif (isOrganizerUserLoggedIn()) {
    $templateParams["titolo"] = "Scalper-Notifiche";
    $templateParams["nome"] = "Notifiche/notificheOrganizzatore.php";
    $idevento = -1;
    if (isset($_SESSION["IdOrganizzatore"])) {
        $id = $_SESSION["IdOrganizzatore"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdOrganizzatore($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }
} elseif (isAdminUserLoggedIn()) {
    $templateParams["titolo"] = "Scalper-Notifiche";
    $templateParams["nome"] = "Notifiche/notificheAmministratore.php";
    $idevento = -1;
    if (isset($_SESSION["IdAmministratore"])) {
        $id = $_SESSION["IdAmministratore"];
        $templateParams["notifiche"] = $dbh->getNotificheByIdAmministratore($id);
        $i = 0;
        foreach ($templateParams["notifiche"] as $notifica) {
            if ($notifica["Letto"] == 0) {
                $i++;
            }
        }
        $templateParams["notificheNONlette"] = $i;
    }
} else {
    $templateParams["titolo"] = "Scalper-Login";
    header("location: login.php");
}


require("template/base.php");
